package com.samsung.checker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by BanK on 12/5/2558.
 */
public class EnterShopCodeActivity extends Activity {
	private EditText codeInput;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entershopcode);
		codeInput = (EditText) findViewById(R.id.shop_code);
		String code = PreferenceManager.getDefaultSharedPreferences(this).getString("shop", "");
		codeInput.setText(code);
		codeInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					checkCode(codeInput.getText().toString());
					return true;
				}
				return false;
			}
		});
		findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkCode(codeInput.getText().toString());
			}
		});
		findViewById(R.id.scan).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(EnterShopCodeActivity.this, ScanActivity.class));
			}
		});
	}

	private void checkCode(final String code) {
		final ProgressDialog p = ProgressDialog.show(this, null, "Loading", true, false);
		String IMEI = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getDeviceId();
		Api.getRestApi().checkShopCode(code, IMEI, new Callback<Api.ResponseShopCode>() {
			@Override
			public void success(Api.ResponseShopCode responseShopCode, Response response) {
				p.dismiss();
				if (responseShopCode.success) {
					PreferenceManager.getDefaultSharedPreferences(EnterShopCodeActivity.this)
							.edit().putString("shop", code).commit();
					// go to registe or checkin
					Intent i = new Intent(EnterShopCodeActivity.this, CheckinActivity.class);
					i.putExtra(CheckinActivity.EXTRA_CHANNEL, responseShopCode.shop.channel_id);
					i.putExtra(CheckinActivity.EXTRA_REGISTER, responseShopCode.register);
					i.putExtra(CheckinActivity.EXTRA_SHOPNAME, responseShopCode.shop.name);
					startActivity(i);
					finish();
				} else {
					Utils.showError(EnterShopCodeActivity.this, responseShopCode.message);
				}
			}

			@Override
			public void failure(RetrofitError error) {
				p.dismiss();
				Utils.showServerError(EnterShopCodeActivity.this);
			}
		});

	}
}
