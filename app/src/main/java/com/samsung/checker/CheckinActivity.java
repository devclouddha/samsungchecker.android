package com.samsung.checker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by BanK on 12/5/2558.
 */
public class CheckinActivity extends Activity {
	public static final String EXTRA_CHANNEL = "EXTRA_CHANNEL",
			EXTRA_REGISTER = "REGISTER",
			EXTRA_SHOPNAME = "SHOPNAME";
	private boolean isRegister;
	private int channelId;
	private String shopName;
	private Callback<Api.ResponseSuccess> checkinCallback;
	private AlertDialog p;
	private boolean enableBack = true;
	private SmartLocation smartLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkin);
		smartLocation = SmartLocation.with(this);
		Intent intent = getIntent();
		isRegister = intent.getBooleanExtra(EXTRA_REGISTER, false);
		channelId = intent.getIntExtra(EXTRA_CHANNEL, 0);
		shopName = intent.getStringExtra(EXTRA_SHOPNAME);
		if (channelId == 0) {
			finish();
		}
		((TextView) findViewById(R.id.shop_name)).setText(shopName);
		final ImageButton checkin = (ImageButton) findViewById(R.id.checkin);
		if (!isRegister) {
			checkin.setImageResource(R.drawable.btn_register);
			((TextView) findViewById(R.id.checkin_detail)).setText(R.string.register_detail);
		}


		checkinCallback = new Callback<Api.ResponseSuccess>() {
			@Override
			public void success(Api.ResponseSuccess responseSuccess, Response response) {
				p.dismiss();
				if (responseSuccess.success) {
					checkin.setVisibility(View.GONE);
					findViewById(R.id.success_text).setVisibility(View.VISIBLE);
					// show success
					if (!isRegister) {
						Toast.makeText(getApplicationContext(), R.string.register_toast, Toast.LENGTH_SHORT).show();
						enableBack = false;
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								startActivity(new Intent(CheckinActivity.this, CheckinActivity.class)
										.putExtra(EXTRA_SHOPNAME, shopName)
										.putExtra(EXTRA_CHANNEL, channelId)
										.putExtra(EXTRA_REGISTER, true));
								finish();
							}
						}, 3000);
					} else {
						((ImageView) findViewById(R.id.checker_text)).setImageResource(R.drawable.checker2);
					}
				} else {
					Utils.showError(CheckinActivity.this, responseSuccess.message);
				}
			}

			@Override
			public void failure(RetrofitError error) {
				p.dismiss();
				Utils.showServerError(CheckinActivity.this);
			}
		};
		findViewById(R.id.checkin).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!smartLocation.location().state().isGpsAvailable()) {
					new AlertDialog.Builder(CheckinActivity.this)
							.setMessage(R.string.please_enable_location)
							.setPositiveButton(R.string.gps_setting, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialogInterface, int i) {
									startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
								}
							})
							.setNegativeButton(R.string.cancel, null)
							.show();
					return;
				}
				final SmartLocation.LocationControl lc = smartLocation.location()
						.config(LocationParams.NAVIGATION)
						.oneFix();

				p = ProgressDialog.show(CheckinActivity.this, null, getResources().getString(R.string.checking_location), true, false);

				final Handler handler = new Handler();
				final Runnable timeoutCallback = new Runnable() {
					@Override
					public void run() {
						lc.stop();
						p.dismiss();
						new AlertDialog.Builder(CheckinActivity.this)
								.setMessage(R.string.location_timeout)
								.setPositiveButton(R.string.ok, null)
								.show();
					}
				};
				lc.start(new OnLocationUpdatedListener() {
					@Override
					public void onLocationUpdated(Location location) {
						handler.removeCallbacks(timeoutCallback);
						String IMEI = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getDeviceId();
						String SERIAL_NUMBER = Build.SERIAL;
						String MODEL_CODE = Build.MODEL;
						String ANDROID_VERSION = Build.VERSION.RELEASE;
						String APP_VERSION_NAME = "0";
						int APP_VERSION_CODE = 0;
						try {
							PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
							APP_VERSION_CODE = info.versionCode;
							APP_VERSION_NAME = info.versionName;
						} catch (PackageManager.NameNotFoundException e) {

						}

						WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
						WifiInfo info = manager.getConnectionInfo();
						String MAC_ADDRESS = info.getMacAddress();
//						if (BuildConfig.DEBUG && MODEL_CODE.equals("Nexus 4")) {
//							Toast.makeText(CheckinActivity.this, "CHECKED IN", Toast.LENGTH_SHORT).show();
//							p.dismiss();
//							return;
//						}
						if (!isRegister) {
							p.setMessage(getResources().getString(R.string.registering));
							Api.getRestApi().register(channelId, MODEL_CODE, IMEI, SERIAL_NUMBER, MAC_ADDRESS, location.getLatitude(), location.getLongitude(), APP_VERSION_NAME, APP_VERSION_CODE, ANDROID_VERSION, checkinCallback);
						} else {
							p.setMessage(getResources().getString(R.string.checking_in));
							Api.getRestApi().checkin(channelId, MODEL_CODE, IMEI, SERIAL_NUMBER, MAC_ADDRESS, location.getLatitude(), location.getLongitude(), APP_VERSION_NAME, APP_VERSION_CODE, ANDROID_VERSION, checkinCallback);
						}
					}
				});
				handler.postDelayed(timeoutCallback, 60000);
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (enableBack)
			super.onBackPressed();
	}
}
