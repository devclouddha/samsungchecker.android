package com.samsung.checker;

import android.app.Activity;
import android.app.AlertDialog;

/**
 * Created by BanK on 12/5/2558.
 */
public class Utils {
	public static void showServerError(Activity a){
		showError(a,a.getResources().getString(R.string.server_error));
	}
	public static void showError(Activity a, String message){
		new AlertDialog.Builder(a)
				.setMessage(message)
				.setPositiveButton(R.string.ok,null)
				.show();
	}
}
