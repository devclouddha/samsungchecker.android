package com.samsung.checker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Bank on 17/8/2558.
 */
public class VersionControl {
	private static final String SERVER = "http://128.199.124.236";
	public static final int APP_ID = 6;
	public static final String DEVICE = "android";

	public static final VersionService getInstance() {
		return new RestAdapter.Builder()
				.setEndpoint(SERVER)
				.setLog(new AndroidLog("BANK"))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.build()
				.create(VersionService.class);
	}

	public static interface VersionService {
		@POST("/update/api/force/checkupdate")
		@FormUrlEncoded
		public void check(@Field("app_id") int appId, @Field("device") String device, Callback<ResponseCheckUpdate> callback);

	}

	public static class ResponseCheckUpdate {
		public boolean success;
		public int version;
		public String link;
	}
}
