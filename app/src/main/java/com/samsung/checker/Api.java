package com.samsung.checker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by BanK on 7/5/2558.
 */
public class Api {
	public static RestApi getRestApi() {
		return new RestAdapter.Builder()
				.setLog(new AndroidLog("BANK"))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint("http://samsungchecker.wisdomcloud.net/api/")
				.build()
				.create(RestApi.class);
	}

	public interface RestApi {
		@POST("/shop/4pin")
		@FormUrlEncoded
		public void checkPin(@Field("pin") String pin, Callback<ResponseSuccess> callback);

		@POST("/shop/shopcode")
		@FormUrlEncoded
		public void checkShopCode(@Field("code") String code, @Field("imei") String imei, Callback<ResponseShopCode> callback);

		@POST("/shop/register")
		@FormUrlEncoded
		public void register(@Field("channel_id") int channel_id, @Field("model") String model_code,
		                     @Field("imei") String imei, @Field("serial") String device_serial,
		                     @Field("mac") String mac, @Field("lat") double lat, @Field("lng") double lng,
		                     @Field("app_version") String appVersion, @Field("version_code") int appVersionCode,
		                     @Field("android_version") String androidVersion, Callback<ResponseSuccess> callback);

		@POST("/shop/checkin")
		@FormUrlEncoded
		public void checkin(@Field("channel_id") int channel_id, @Field("model") String model_code,
		                    @Field("imei") String imei, @Field("serial") String device_serial, @Field("mac") String mac,
		                    @Field("lat") double lat, @Field("lng") double lng,
		                    @Field("app_version") String appVersion, @Field("version_code") int appVersionCode,
		                    @Field("android_version") String androidVersion, Callback<ResponseSuccess> callback);
	}

	public static class ResponseSuccess {
		public boolean success;
		public String message;
	}

	public static class ResponseShopCode extends ResponseSuccess {
		public Shop shop;
		public boolean register;

		public static class Shop {
			public int channel_id, group_id, area_id;
			public String code, name;
		}
	}


}
