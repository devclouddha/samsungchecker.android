package com.samsung.checker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class EnterPinActivity extends AppCompatActivity {
    private EditText pinInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        checkForUpdate();
        if (!BuildConfig.DEBUG && !Build.MANUFACTURER.toLowerCase().equals("samsung")) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(R.string.not_samsung)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .show();
            return;
        }

        setContentView(R.layout.activity_enterpin);
        pinInput = (EditText) findViewById(R.id.pin_code);
        pinInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String pin = pinInput.getText().toString();
                    checkPin(pin);
                    return true;
                }
                return false;
            }
        });
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = pinInput.getText().toString();
                checkPin(pin);
            }
        });
    }

    private void checkForUpdate() {

        // check version
        final ProgressDialog p = ProgressDialog.show(this, null, getString(R.string.checking_version), true, false);
        VersionControl.getInstance().check(VersionControl.APP_ID, VersionControl.DEVICE, new Callback<VersionControl.ResponseCheckUpdate>() {
            @Override
            public void success(final VersionControl.ResponseCheckUpdate responseCheckUpdate, final Response response) {
                p.dismiss();
                if (responseCheckUpdate.success) {
                    try {
                        PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
                        if (info.versionCode < responseCheckUpdate.version) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(EnterPinActivity.this)
                                    .setMessage(R.string.version_outdate)
                                    .setCancelable(false)
                                    .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                        }
                                    });
                            if (!responseCheckUpdate.link.isEmpty()) {
                                builder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(responseCheckUpdate.link)));
                                    }
                                });
                            }
                            builder.show();
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        showUpdateError();
                    }
                } else {
                    showUpdateError();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                p.dismiss();
                showUpdateError();
            }
        });

    }

    private void showUpdateError() {

        new AlertDialog.Builder(EnterPinActivity.this)
                .setTitle(R.string.error)
                .setMessage(R.string.server_error)
                .setCancelable(false)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkForUpdate();
                    }
                })
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }).show();
    }

    private void checkPin(final String pin) {
        final ProgressDialog p = ProgressDialog.show(this, null, "Loading", true, false);
        Api.getRestApi().checkPin(pin, new Callback<Api.ResponseSuccess>() {
            @Override
            public void success(Api.ResponseSuccess responseSuccess, Response response) {
                p.dismiss();
                if (responseSuccess.success) {
                    startActivity(new Intent(EnterPinActivity.this, EnterShopCodeActivity.class));
                    finish();
                } else {
                    Utils.showError(EnterPinActivity.this, responseSuccess.message);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                p.dismiss();
                Utils.showServerError(EnterPinActivity.this);
            }
        });
    }
}
