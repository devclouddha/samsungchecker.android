package com.samsung.checker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import eu.livotov.labs.android.camview.ScannerLiveView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by BanK on 12/5/2558.
 */
public class ScanActivity extends Activity {
    private ScannerLiveView scanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        scanner = (ScannerLiveView) findViewById(R.id.scanner);
        scanner.setHudVisible(false);
        scanner.setScannerViewEventListener(new ScannerLiveView.ScannerViewEventListener() {
            @Override
            public void onScannerStarted(ScannerLiveView scanner) {

            }

            @Override
            public void onScannerStopped(ScannerLiveView scanner) {

            }

            @Override
            public void onScannerError(Throwable err) {

            }

            @Override
            public void onCodeScanned(String data) {
                scanner.stopScanner();
                checkCode(data);
            }
        });
    }

    private void checkCode(final String code) {
        final ProgressDialog p = ProgressDialog.show(this, null, "Loading", true, false);
        String IMEI = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getDeviceId();
        Api.getRestApi().checkShopCode(code, IMEI, new Callback<Api.ResponseShopCode>() {
            @Override
            public void success(Api.ResponseShopCode responseShopCode, Response response) {
                p.dismiss();
                if (responseShopCode.success) {
                    // go to registe or checkin
                    PreferenceManager.getDefaultSharedPreferences(ScanActivity.this)
                            .edit().putString("shop", code).commit();
                    Intent i = new Intent(ScanActivity.this, CheckinActivity.class);
                    i.putExtra(CheckinActivity.EXTRA_CHANNEL, responseShopCode.shop.channel_id);
                    i.putExtra(CheckinActivity.EXTRA_REGISTER, responseShopCode.register);
                    i.putExtra(CheckinActivity.EXTRA_SHOPNAME, responseShopCode.shop.name);
                    startActivity(i);
                    finish();
                } else {
                    new AlertDialog.Builder(ScanActivity.this)
                            .setMessage(R.string.scan_error)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    scanner.startScanner();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                p.dismiss();
                Utils.showServerError(ScanActivity.this);
                new AlertDialog.Builder(ScanActivity.this)
                        .setMessage(R.string.server_error)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                scanner.startScanner();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scanner.startScanner();
            }
        }, 500);
    }

    @Override
    protected void onPause() {
        scanner.stopScanner();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        scanner.stopScanner();
        super.onBackPressed();
    }
}
